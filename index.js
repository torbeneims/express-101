const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser')


app.use(cors({origin: '*'}));
app.use(bodyParser.json())


app.get('/', (req, res) => {
    const name = process.env.NAME || 'World';
    res.send(`Hello ${name}!`);
});

app.get('/hi/:param?', (req, res) => {
    res.status(200).send({param: req.params.param ?? "", query: req.query.query ?? ""});
})

const port = process.env.PORT || 8080;
app.listen(port, () => {
    console.log(`helloworld: listening on port ${port}`);
});
